var start = Date.now();

var forge = require('node-forge');


//=================== ENCRYTION SECTION =========================
var salt = forge.random.getBytesSync(128);
console.log("Salt: " + salt);

var key = forge.pkcs5.pbkdf2("password1", salt, 40, 16);
console.log("Key: " + key);

var iv = forge.random.getBytesSync(16);
console.log("iv: " + iv);
console.log(" ");

var cipher = forge.cipher.createCipher('AES-CBC', key);
cipher.start({iv: iv});
cipher.update(forge.util.createBuffer("My Swiss Bank Account Number is: 887222323-IDS-998" ));
cipher.finish();
var cipherText = forge.util.encode64(cipher.output.getBytes());

//convert the encrypted text from gibberish (ie binary) to base64 as it will screw up the databse

var myObj = {
    cipher_text: cipherText,
    salt: forge.util.encode64(salt),
    iv: forge.util.encode64(iv)
};


//window.localStorage.setItem("encrypted_item", JSON.stringify(myObj));
var encryptedItem = JSON.stringify(myObj);
console.log("encryptedItem: " + encryptedItem);
console.log("");
console.log("enc cipher text: " + myObj.cipher_text);
console.log("enc salt: " + myObj.salt);
console.log("enc iv: " + myObj.iv);
console.log("");




//=================== DECRYTION SECTION =========================
var saltD = forge.util.decode64(myObj.salt)
var ivD = forge.util.decode64(myObj.iv);
var keyD = forge.pkcs5.pbkdf2("password1", saltD, 40, 16);
var decipher = forge.cipher.createDecipher('AES-CBC', keyD);

decipher.start({iv: ivD});
decipher.update(forge.util.createBuffer(forge.util.decode64(myObj.cipher_text)));
decipher.finish();
var decipheredText = decipher.output.toString();
console.log("decipheredText: " + decipheredText);

var end = Date.now();
var totalTime = end - start;

//console.log("Start Time: " + start);
//console.log("End Time:   " + end);
//console.log("------------------");
console.log("Total Time: " + totalTime + "ms");
