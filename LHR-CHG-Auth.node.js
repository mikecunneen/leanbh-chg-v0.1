//turns off the rejection of self signed ssl cert
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var request = require('request');

request({
    url: 'https://leanbh.stephenlane.me/gatekeeper/rs/authenticate/login', //URL to hit
    qs: {username: 'admin', password: 'sysadmin', realm: 'tolven'}, //Query string data
    method: 'POST', //Specify the method
    headers: { //We can define headers too
        'Content-Type': 'application/x-www-form-urlencoded'
    }


},


function(error, response, body){

    if(error) {
        console.log("Authentication Failed!");
        console.log(error);
    }
    else {


              console.log("We are Logged in! Now, parse the cookie...");
              console.log("  ");


              //Parsing Cookie Logic
              //eg  SSOCookie=a59e9799-6cd7-4085-9c21-9ecebde4226e_lq1Fx9NpIs6aLXBFZsBbJw%3D%3D; Domain=.
              //remove everything from ; onwards BUT Dont remove SSOCookie= (I thought this initially)


               //get the Cookie returned from Tolven
              var newCookie = response.headers['set-cookie'];
              console.log("Here's the Raw Cookie: " + newCookie);

              var a = newCookie.toString();
              var b = a.toString().split(";");
              console.log("Here's the Parsed Cookie: " + b[0]);

              var finalCookieString = b[0];
              console.log("  ");


              //var baseApiUrl = "https://leanbh.stephenlane.me/api/";

              var getTime = "https://leanbh.stephenlane.me/api/guest/time";
              var getHeaders = "https://leanbh.stephenlane.me/api/guest/headers";



              var jar = request.jar();
              var cookieZ = request.cookie(finalCookieString);
              //jar.add(cookieZ); >> doesn't work
              jar.setCookie(cookieZ, getTime, function(error, cookie) {});





                                //*****Starting second request (to Tolven Api)
                                request({
                                    url: getTime, //URL to hit
                                    method: 'GET', //Specify the method
                                    headers: { //We can define headers too
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    },
                                    jar:jar


                                },

                                function(error, response, body){
                                    if(error) {
                                        console.log(error);
                                    } else {

                                       //should post response from api
                                        console.log("Server Status Code + Body: " + response.statusCode, body);

                                      }

                                  }) //end of nested (2nd) request function








    } //end of first request function
})
