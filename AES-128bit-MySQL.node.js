//This app is a model of how to encrypt data on the phone... post to a database and then decrypt when you get it back

//requires/imports...
var forge = require('node-forge');




//=================== ENCRYTION SECTION =========================
var salt = forge.random.getBytesSync(128);
var key = forge.pkcs5.pbkdf2("password1", salt, 40, 16);
var iv = forge.random.getBytesSync(16);

var cipher = forge.cipher.createCipher('AES-CBC', key);
cipher.start({iv: iv});
cipher.update(forge.util.createBuffer("My Swiss Bank Account Number is: 887222323-IDS-998" ));
cipher.finish();
var cipherText = forge.util.encode64(cipher.output.getBytes());

//convert the encrypted text from binary to base64 as easier to store.
var myObj = {
    cipher_text: cipherText,
    salt: forge.util.encode64(salt),
    iv: forge.util.encode64(iv)
};

var encryptedItem = JSON.stringify(myObj);





//=================== DECRYTION SECTION =========================
var saltD = forge.util.decode64(myObj.salt)
var ivD = forge.util.decode64(myObj.iv);
var keyD = forge.pkcs5.pbkdf2("password1", saltD, 40, 16);
var decipher = forge.cipher.createDecipher('AES-CBC', keyD);

decipher.start({iv: ivD});
decipher.update(forge.util.createBuffer(forge.util.decode64(myObj.cipher_text)));
decipher.finish();
var decipheredText = decipher.output.toString();

console.log("DeCrypted Text: " + decipheredText);




//===================  NOW, POST THE ENCRYPED DATA TO A MySqldb (using nodejs-mysql lib) =========================
//need to install mysql locally first...
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'ppjsml444',
  database : 'emp'
});

connection.connect();

connection.query('SELECT * from city', function(err, rows, fields) {
  if (!err)
    console.log('The solution is: ', rows);
  else
    console.log('Error while performing Query.');
});

connection.end();
